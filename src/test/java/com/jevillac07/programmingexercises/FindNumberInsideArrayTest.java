package com.jevillac07.programmingexercises;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class FindNumberInsideArrayTest {

  @ParameterizedTest
  @ValueSource(ints = {7, 24, 510, 69, 2040})
  @DisplayName("Find a number inside an array")
  void findNumberInsideArray(int findNumber) {
    int[] numbers = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 15, 24, 35, 68, 150, 420, 510, 1024};
    boolean answer = false;
    boolean overflow = findNumber > numbers[numbers.length - 1] || findNumber < numbers[0];

    if (overflow) {
      System.out.println("Answer - overflow: " + answer);
    } else {
      int newRangeMin = 0;
      int newRangeMax = numbers.length - 1;
      int index; int numberPosition;
      int leftovers = 99;

      while (leftovers >= 4) {
        index = (newRangeMax + newRangeMin) / 2;
        numberPosition = numbers[index];
        boolean isValid = findNumber > numberPosition;

        if (isValid) {
          newRangeMin = index;
        } else {
          newRangeMax = index;
        }

        leftovers = newRangeMax - newRangeMin;
      }

      for (int i = newRangeMin; i <= newRangeMax; i++) {
        if (findNumber == numbers[i]) {
          answer = true;
          break;
        }

        if (findNumber < numbers[i]) {
          break;
        }

        System.out.println("* " + findNumber + " equals " + numbers[i] + "?");
      }

      System.out.println("Answer: " + answer);
    }
  }
}
