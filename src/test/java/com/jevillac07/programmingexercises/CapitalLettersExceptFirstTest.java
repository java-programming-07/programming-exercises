package com.jevillac07.programmingexercises;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

class CapitalLettersExceptFirstTest {

	@Test
  @DisplayName("Capital letters except the first")
	void capitalLettersExceptFirst() {
    String input = "new date is null";
    String output = "nEW dATE iS nULL";

    String upperSentence = input.toUpperCase();
    char space = ' ';
    StringBuilder newSentence = new StringBuilder();

    for (int i = 0; i < upperSentence.length(); i++) {
      if (i == 0 || space == upperSentence.charAt(i - 1)) {
        newSentence.append(upperSentence.toLowerCase().charAt(i));
      } else {
        newSentence.append(upperSentence.charAt(i));
      }
    }

    Assertions.assertEquals(newSentence.toString(), output);
	}

}
