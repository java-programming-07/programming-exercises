package com.jevillac07.programmingexercises;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProgrammingExercisesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProgrammingExercisesApplication.class, args);
	}

}
